output "groups" {
  value = local.groups
}
output "runner_token" {
  value     = local.runner_token
  sensitive = true
}
