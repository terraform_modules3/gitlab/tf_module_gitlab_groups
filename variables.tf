variable "groups" {
  description = <<EOT
  https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group

  Notes:
  On SaaS gitlab (https://gitlab.com/) groups cannot be created in root vi API (https://gitlab.com/gitlab-org/gitlab/-/issues/244345#note_1021388399). Create group manually and then import to tf: tf import 'gitlab_group.group["group-name"]' group-path
  If project_id in projects_access is specified, project_full_name is ignored
  If group_id in groups_access is specified, group_full_path is ignored

  «expires_at» format: "YYYY-MM-DD"

  Example:
  groups = [
    { 0 = {
      name        = "group-name"
      parent_id   = 123
      runners = [
        { 1 = {
          locked          = false
          maximum_timeout = 600
          paused          = false
          access_level    = "not_protected" # not_protected, ref_protected.
          description     = "Docker executor"
          tag_list        = ["docker"]
          untagged        = true
        } },
        { 2 = {} },
      ]
      env_vars = [
        { 1 = {
          key               = "VAR1"
          value             = var.var1_secret_value
          masked            = true
        } },
      ]
    } },
    { 1 = {
      name        = "group-name2"
      parent_group_full_path  = "group1"
      projects_access = [
        {
          project_id = 6
          access_level = "developer"
        },
        {
          project_full_name = "group/subgroup2/project22"
          access_level      = "guest"
        },
      ]
      groups_access = [
        {
          group_id     = 20
          access_level = "developer"
        },
        {
          group_full_path = "group/subgroup2"
          access_level = "guest"
          expires_at   = "2099-12-31"
        }
      ],
    } },
  ]
  EOT
  type = list(map(
    object({
      name                               = string
      parent_id                          = optional(number)
      parent_group_full_path             = optional(string)
      path                               = optional(string, ""),
      description                        = optional(string, ""),
      visibility_level                   = optional(string, "private"),
      auto_devops_enabled                = optional(bool, false),
      default_branch_protection          = optional(number, 2),
      emails_disabled                    = optional(bool, false),
      extra_shared_runners_minutes_limit = optional(number, 0),
      subgroup_creation_level            = optional(string, "maintainer"),
      lfs_enabled                        = optional(bool, true),
      ip_restriction_ranges              = optional(list(string), []),
      membership_lock                    = optional(bool, false),
      mentions_disabled                  = optional(bool, false),
      prevent_forking_outside_group      = optional(bool, false),
      project_creation_level             = optional(string, "maintainer"),
      request_access_enabled             = optional(bool, true),
      require_two_factor_authentication  = optional(bool, false),
      share_with_group_lock              = optional(bool, false),
      shared_runners_minutes_limit       = optional(number, 0),
      two_factor_grace_period_hours      = optional(number, 48),

      projects_access = optional(list(object({
        project_full_name = optional(string)
        project_id        = optional(number)
        access_level      = optional(string, "guest")
      })), []),
      groups_access = optional(list(object({
        group_full_path = optional(string)
        group_id        = optional(number)
        access_level    = optional(string, "guest")
        expires_at      = optional(string, null) # Format: "YYYY-MM-DD"
      })), []),
      runners = optional(list(map(object({
        locked          = optional(bool, null)
        maximum_timeout = optional(number, null)
        paused          = optional(bool, null)
        access_level    = optional(string, null) # not_protected, ref_protected.
        description     = optional(string, null)
        tag_list        = optional(list(string), null)
        untagged        = optional(bool, null)
      }))), [])
      env_vars = optional(list(map(object({
        key               = string
        value             = string
        description       = optional(string, null)
        environment_scope = optional(string, null) # not_protected, ref_protected.
        masked            = optional(bool, null)
        raw               = optional(bool, null)
        protected         = optional(bool, null)
        variable_type     = optional(string, null) # env_var | file
      }))), [])
    })
  ))
  default = []
}
