locals {
  projects_access = toset(flatten([
    for group in var.groups : [
      for project_access in values(group)[0].projects_access : merge(
        { group_index = keys(group)[0] },
        {
          project_id   = project_access.project_id == null ? tonumber(data.gitlab_project.project[project_access.project_full_name].id) : project_access.project_id
          access_level = project_access.access_level
        }
      )
    ]
  ]))

  projects_full_names = toset(flatten([
    for group in var.groups : [
      for project_access in values(group)[0].projects_access :
      project_access.project_full_name
      if project_access.project_id == null
    ]
  ]))
}

resource "gitlab_project_share_group" "access" {
  for_each = { for project_access in local.projects_access : "${project_access.group_index}:${project_access.project_id}" => project_access }

  project      = each.value.project_id
  group_id     = gitlab_group.group[each.value.group_index].id
  group_access = each.value.access_level

  depends_on = [gitlab_group.group]
}
