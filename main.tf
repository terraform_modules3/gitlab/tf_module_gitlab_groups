locals {
  # for output
  groups = {
    for index, group_info in gitlab_group.group :
    index => {
      name       = group_info.name
      url        = group_info.web_url
      visibility = group_info.visibility_level
      path       = group_info.path
      full_path  = group_info.full_path
      parent_id  = group_info.parent_id
      id         = group_info.id
    }
  }
}

resource "gitlab_group" "group" {
  for_each = { for group in var.groups : keys(group)[0] => values(group)[0] }

  name                               = each.value.name
  path                               = each.value.path == "" ? lower(replace(each.value.name, "/\\s/", "_")) : each.value.path
  description                        = each.value.description
  visibility_level                   = each.value.visibility_level
  auto_devops_enabled                = each.value.auto_devops_enabled
  parent_id                          = each.value.parent_id == null ? tonumber(data.gitlab_group.group[each.key].id) : each.value.parent_id
  default_branch_protection          = each.value.default_branch_protection
  emails_disabled                    = each.value.emails_disabled
  extra_shared_runners_minutes_limit = each.value.extra_shared_runners_minutes_limit
  subgroup_creation_level            = each.value.subgroup_creation_level
  lfs_enabled                        = each.value.lfs_enabled
  ip_restriction_ranges              = each.value.ip_restriction_ranges
  membership_lock                    = each.value.membership_lock
  mentions_disabled                  = each.value.mentions_disabled
  prevent_forking_outside_group      = each.value.prevent_forking_outside_group
  project_creation_level             = each.value.project_creation_level
  request_access_enabled             = each.value.request_access_enabled
  require_two_factor_authentication  = each.value.require_two_factor_authentication
  share_with_group_lock              = each.value.share_with_group_lock
  shared_runners_minutes_limit       = each.value.shared_runners_minutes_limit
  two_factor_grace_period            = each.value.two_factor_grace_period_hours

  lifecycle {
    prevent_destroy = true
  }
}
