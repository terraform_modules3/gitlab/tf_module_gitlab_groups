locals {
  groups_access = toset(flatten([
    for group in var.groups : [
      for group_access in values(group)[0].groups_access : merge(
        { group_index = keys(group)[0] },
        {
          group_id     = group_access.group_id == null ? tonumber(data.gitlab_group.group_access[group_access.group_full_path].id) : group_access.group_id
          access_level = group_access.access_level
          expires_at   = group_access.expires_at
        }
      )
    ]
  ]))

  groups_full_paths = toset(flatten([
    for group in var.groups : [
      for group_access in values(group)[0].groups_access :
      group_access.group_full_path
      if group_access.group_id == null
    ]
  ]))
}

resource "gitlab_group_share_group" "access" {
  for_each = { for group_access in local.groups_access : "${group_access.group_index}:${group_access.group_id}" => group_access }

  group_id       = each.value.group_id
  share_group_id = gitlab_group.group[each.value.group_index].id
  group_access   = each.value.access_level
  expires_at     = each.value.expires_at

  depends_on = [gitlab_group.group]
}
