# for gitlab groups parent_id
data "gitlab_group" "group" {
  for_each = {
    for group in var.groups : keys(group)[0] => values(group)[0] if values(group)[0].parent_id == null && values(group)[0].parent_group_full_path != ""
  }
  full_path = each.value.parent_group_full_path
}

# for group access rules
data "gitlab_group" "group_access" {
  for_each  = local.groups_full_paths
  full_path = each.value
}

data "gitlab_project" "project" {
  for_each            = local.projects_full_names
  path_with_namespace = each.value
}
