resource "gitlab_user_runner" "group_runner" {
  for_each = { for runner in local.runners : "${runner.group_index}:${runner.runner_index}" => runner }

  runner_type = "group_type"
  group_id    = gitlab_group.group[each.value.group_index].id

  locked          = each.value.locked
  maximum_timeout = each.value.maximum_timeout
  paused          = each.value.paused
  access_level    = each.value.access_level
  description     = each.value.description
  tag_list        = each.value.tag_list
  untagged        = each.value.untagged

  depends_on = [
    gitlab_group.group
  ]
}

locals {
  runners = flatten([
    for group in var.groups : [
      for runner in group[keys(group)[0]].runners : {
        group_index     = keys(group)[0]
        runner_index    = keys(runner)[0]
        locked          = runner[keys(runner)[0]].locked
        maximum_timeout = runner[keys(runner)[0]].maximum_timeout
        paused          = runner[keys(runner)[0]].paused
        access_level    = runner[keys(runner)[0]].access_level
        description     = runner[keys(runner)[0]].description
        tag_list        = runner[keys(runner)[0]].tag_list
        untagged        = runner[keys(runner)[0]].untagged
      }
    ]
  ])
}

locals {
  # for output
  runner_token = {
    for index, runner_info in gitlab_user_runner.group_runner :
    index => {
      token = runner_info.token
    }
  }
}
