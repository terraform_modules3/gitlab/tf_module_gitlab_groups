# Gitlab groups Terraform module
Module to create and manage groups in Gitlab

Allows to invite groups to projects and other groups

## Usage example
```hcl
module "groups" {
  source = "git::https://gitlab.com/terraform_modules3/gitlab/tf_module_gitlab_groups.git?ref=0.1.0"

  groups = [
    { 0 = {
      name        = "group-name"
      parent_id   = 123
      runners = [
        { 1 = {
          locked          = false
          maximum_timeout = 600
          paused          = false
          access_level    = "not_protected" # not_protected, ref_protected.
          description     = "Docker executor"
          tag_list        = ["docker"]
          untagged        = true
        } },
        { 2 = {} },
      ]
      env_vars = [
        { 1 = {
          key               = "VAR1"
          value             = var.var1_secret_value
          masked            = true
        } },
      ]
    } },
    { 1 = {
      name        = "group-name2"
      parent_group_full_path  = "group1"
      projects_access = [
        {
          project_id = 6
          access_level = "developer"
        },
        {
          project_full_name = "group/subgroup2/project22"
          access_level      = "guest"
        },
      ],
      groups_access = [
        {
          group_id     = 20
          access_level = "developer"
        },
        {
          group_full_path = "group/subgroup2"
          access_level = "guest"
          expires_at   = "2099-12-31"
        }
      ],
    } },
  ]
}

```

## Output example
```hcl
groups = {
  "group-name" = {
    "full_path" = "group-name"
    "id" = "6"
    "name" = "group-name"
    "parent_id" = 0
    "path" = "group-name"
    "url" = "https://gitlab.example.com/groups/group-name"
    "visibility" = "private"
  }
  "group3/group-name2" = {
    "full_path" = "group3/group-name2"
    "id" = "23"
    "name" = "group-name2"
    "parent_id" = 6
    "path" = "group-name2"
    "url" = "https://gitlab.example.com/groups/group3/group-name2"
    "visibility" = "private"
  }
}
runners_tokens = {
  "1:1" = {
    "token" = "glrt-z…d"
  }
  "3:1" = {
    "token" = "glrt-U…z"
  }
  "3:2" = {
    "token" = "glrt-4…T"
  }
}
```

### if «path» property is omitted, it will be generated from «name» property by lowercasing and replacing whitespaces with underscore symbol
```console
lower(replace(each.value.name, "/\\s/", "_"))
```
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.7 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 16.9.1, < 17.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | 16.11.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_group.group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group) | resource |
| [gitlab_group_share_group.access](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_share_group) | resource |
| [gitlab_group_variable.variable](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable) | resource |
| [gitlab_project_share_group.access](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_share_group) | resource |
| [gitlab_user_runner.group_runner](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/user_runner) | resource |
| [gitlab_group.group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |
| [gitlab_group.group_access](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |
| [gitlab_project.project](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/project) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_groups"></a> [groups](#input\_groups) | https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group<br><br>  Notes:<br>  On SaaS gitlab (https://gitlab.com/) groups cannot be created in root vi API (https://gitlab.com/gitlab-org/gitlab/-/issues/244345#note_1021388399). Create group manually and then import to tf: tf import 'gitlab\_group.group["group-name"]' group-path<br>  If project\_id in projects\_access is specified, project\_full\_name is ignored<br>  If group\_id in groups\_access is specified, group\_full\_path is ignored<br><br>  «expires\_at» format: "YYYY-MM-DD"<br><br>  Example:<br>  groups = [<br>    { 0 = {<br>      name        = "group-name"<br>      parent\_id   = 123<br>      runners = [<br>        { 1 = {<br>          locked          = false<br>          maximum\_timeout = 600<br>          paused          = false<br>          access\_level    = "not\_protected" # not\_protected, ref\_protected.<br>          description     = "Docker executor"<br>          tag\_list        = ["docker"]<br>          untagged        = true<br>        } },<br>        { 2 = {} },<br>      ]<br>      env\_vars = [<br>        { 1 = {<br>          key               = "VAR1"<br>          value             = var.var1\_secret\_value<br>          masked            = true<br>        } },<br>      ]<br>    } },<br>    { 1 = {<br>      name        = "group-name2"<br>      parent\_group\_full\_path  = "group1"<br>      projects\_access = [<br>        {<br>          project\_id = 6<br>          access\_level = "developer"<br>        },<br>        {<br>          project\_full\_name = "group/subgroup2/project22"<br>          access\_level      = "guest"<br>        },<br>      ]<br>      groups\_access = [<br>        {<br>          group\_id     = 20<br>          access\_level = "developer"<br>        },<br>        {<br>          group\_full\_path = "group/subgroup2"<br>          access\_level = "guest"<br>          expires\_at   = "2099-12-31"<br>        }<br>      ],<br>    } },<br>  ] | <pre>list(map(<br>    object({<br>      name                               = string<br>      parent_id                          = optional(number)<br>      parent_group_full_path             = optional(string)<br>      path                               = optional(string, ""),<br>      description                        = optional(string, ""),<br>      visibility_level                   = optional(string, "private"),<br>      auto_devops_enabled                = optional(bool, false),<br>      default_branch_protection          = optional(number, 2),<br>      emails_disabled                    = optional(bool, false),<br>      extra_shared_runners_minutes_limit = optional(number, 0),<br>      subgroup_creation_level            = optional(string, "maintainer"),<br>      lfs_enabled                        = optional(bool, true),<br>      ip_restriction_ranges              = optional(list(string), []),<br>      membership_lock                    = optional(bool, false),<br>      mentions_disabled                  = optional(bool, false),<br>      prevent_forking_outside_group      = optional(bool, false),<br>      project_creation_level             = optional(string, "maintainer"),<br>      request_access_enabled             = optional(bool, true),<br>      require_two_factor_authentication  = optional(bool, false),<br>      share_with_group_lock              = optional(bool, false),<br>      shared_runners_minutes_limit       = optional(number, 0),<br>      two_factor_grace_period_hours      = optional(number, 48),<br><br>      projects_access = optional(list(object({<br>        project_full_name = optional(string)<br>        project_id        = optional(number)<br>        access_level      = optional(string, "guest")<br>      })), []),<br>      groups_access = optional(list(object({<br>        group_full_path = optional(string)<br>        group_id        = optional(number)<br>        access_level    = optional(string, "guest")<br>        expires_at      = optional(string, null) # Format: "YYYY-MM-DD"<br>      })), []),<br>      runners = optional(list(map(object({<br>        locked          = optional(bool, null)<br>        maximum_timeout = optional(number, null)<br>        paused          = optional(bool, null)<br>        access_level    = optional(string, null) # not_protected, ref_protected.<br>        description     = optional(string, null)<br>        tag_list        = optional(list(string), null)<br>        untagged        = optional(bool, null)<br>      }))), [])<br>      env_vars = optional(list(map(object({<br>        key               = string<br>        value             = string<br>        description       = optional(string, null)<br>        environment_scope = optional(string, null) # not_protected, ref_protected.<br>        masked            = optional(bool, null)<br>        raw               = optional(bool, null)<br>        protected         = optional(bool, null)<br>        variable_type     = optional(string, null) # env_var | file<br>      }))), [])<br>    })<br>  ))</pre> | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_groups"></a> [groups](#output\_groups) | n/a |
| <a name="output_runner_token"></a> [runner\_token](#output\_runner\_token) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
