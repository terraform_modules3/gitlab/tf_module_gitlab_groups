resource "gitlab_group_variable" "variable" {
  for_each = { for env_var in local.env_vars : "${env_var.group_index}:${env_var.env_var_index}" => env_var }

  group = gitlab_group.group[each.value.group_index].id
  key   = each.value.key
  value = each.value.value


  description       = each.value.description       # ""
  environment_scope = each.value.environment_scope # "*"
  masked            = each.value.masked            # true   #
  raw               = each.value.raw               # false  # true — not expended
  variable_type     = each.value.variable_type     # "file" # env_var | file
  protected         = each.value.protected         # false

  depends_on = [
    gitlab_group.group
  ]
}

locals {
  env_vars = flatten([
    for group in var.groups : [
      for env_var in group[keys(group)[0]].env_vars : {
        group_index       = keys(group)[0]
        env_var_index     = keys(env_var)[0]
        key               = env_var[keys(env_var)[0]].key
        value             = env_var[keys(env_var)[0]].value
        description       = env_var[keys(env_var)[0]].description
        environment_scope = env_var[keys(env_var)[0]].environment_scope
        masked            = env_var[keys(env_var)[0]].masked
        raw               = env_var[keys(env_var)[0]].raw
        variable_type     = env_var[keys(env_var)[0]].variable_type
        protected         = env_var[keys(env_var)[0]].protected
      }
    ]
  ])
}
